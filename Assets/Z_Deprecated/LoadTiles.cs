﻿#pragma warning disable 0414, 0219

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: LoadTiles 
/// </summary>
public class LoadTiles : MonoBehaviour {
    #region Fields

    public TextAsset[] mapInfo;
    public GameObject tempCube;
    Sprite[] sprites;

    public float PixelsPerUnit {
        get {
            if (sprites[0] != null) {
                return sprites[0].pixelsPerUnit;
            } else {
                return 1;
            }
        }
    }

    GameObject tileParent;

    #endregion

    void Start() {
        tileParent = new GameObject();
        
        LoadMapFromXML(Random.Range(0, mapInfo.Length));
    }

    public void LoadMapFromXML(int mapNumber) {
        sprites = Resources.LoadAll<Sprite>("Maps/forest_tiles");

        XmlDocument xmlDoc = new XmlDocument();
        print(mapNumber);

        tileParent.name = "map: " + mapNumber;

        xmlDoc.LoadXml(mapInfo[mapNumber].text);

        Camera.main.transform.position = new Vector3(7f, 4.5f, -10);

        XmlNodeList layerNames = xmlDoc.GetElementsByTagName("layer");

        XmlNode tilesetInfo = xmlDoc.SelectSingleNode("map");

        //float tileWidth = int.Parse(tilesetInfo.Attributes["width"].Value) / (float) 1000;
        float tileWidth = int.Parse(tilesetInfo.Attributes["tilewidth"].Value) / PixelsPerUnit;
        float tileHeight = int.Parse(tilesetInfo.Attributes["tileheight"].Value) / PixelsPerUnit;

        foreach (XmlNode layerInfo in layerNames) {
            int layerWidth = int.Parse(layerInfo.Attributes["width"].Value);
            int layerHeight = int.Parse(layerInfo.Attributes["height"].Value);

            if (layerInfo.Attributes["name"].Value == "Background") {
                XmlNode tempNode = layerInfo.SelectSingleNode("data");

                int mapX, mapY;
                mapX = mapY = 0;

                foreach (XmlNode tile in tempNode.SelectNodes("tile")) {
                    int spriteValue = int.Parse(tile.Attributes["gid"].Value);

                    if (spriteValue > 0) {
                        GameObject temp = new GameObject(string.Format("tile {0} {1}", mapX, mapY));
                        SpriteRenderer renderer = temp.AddComponent<SpriteRenderer>();
                        renderer.sprite = sprites[spriteValue - 1];

                        float locationX = tileWidth * mapX;
                        float locationY = tileHeight * mapY;
                        Vector3 instanceCoord = new Vector3(locationX, locationY, 0f);

                        temp.transform.position = instanceCoord;

                        renderer.sortingLayerName = layerInfo.Attributes["name"].Value;

                        //Instantiate(temp, instanceCoord, Quaternion.identity);
                    }

                    mapX++;

                    if (mapX % layerWidth == 0) {
                        //last tile in row
                        mapY++;
                        mapX = 0;
                    }
                }
            } else if (layerInfo.Attributes["name"].Value == "Obstacles") {
                XmlNode tempNode = layerInfo.SelectSingleNode("data");

                int mapX, mapY;
                mapX = mapY = 0;

                foreach (XmlNode tile in tempNode.SelectNodes("tile")) {
                    int spriteValue = int.Parse(tile.Attributes["gid"].Value);

                    if (spriteValue > 0) {
                        GameObject temp = new GameObject(string.Format("tile {0} {1}", mapX, mapY));
                        SpriteRenderer renderer = temp.AddComponent<SpriteRenderer>();
                        renderer.sprite = sprites[spriteValue - 1];

                        float locationX = tileWidth * mapX;
                        float locationY = tileHeight * mapY;
                        Vector3 instanceCoord = new Vector3(locationX, locationY, 0f);

                        temp.transform.position = instanceCoord;

                        renderer.sortingLayerName = layerInfo.Attributes["name"].Value;
                        temp.AddComponent<BoxCollider2D>();
                        Rigidbody2D ha = temp.AddComponent<Rigidbody2D>();
                        ha.gravityScale = 0;
                        ha.constraints = RigidbodyConstraints2D.FreezeAll;

                        //Instantiate(temp, instanceCoord, Quaternion.identity);
                    }

                    mapX++;

                    if (mapX % layerWidth == 0) {
                        //last tile in row
                        mapY++;
                        mapX = 0;
                    }
                }
            } else if (layerInfo.Attributes["name"].Value == "Foreground") {
                XmlNode tempNode = layerInfo.SelectSingleNode("data");

                int mapX, mapY;
                mapX = mapY = 0;

                foreach (XmlNode tile in tempNode.SelectNodes("tile")) {
                    int spriteValue = int.Parse(tile.Attributes["gid"].Value);

                    if (spriteValue > 0) {
                        GameObject temp = new GameObject(string.Format("tile {0} {1}", mapX, mapY));
                        SpriteRenderer renderer = temp.AddComponent<SpriteRenderer>();
                        renderer.sprite = sprites[spriteValue - 1];

                        float locationX = tileWidth * mapX;
                        float locationY = tileHeight * mapY;
                        Vector3 instanceCoord = new Vector3(locationX, locationY, 0f);

                        temp.transform.position = instanceCoord;

                        renderer.sortingLayerName = layerInfo.Attributes["name"].Value;

                        //Instantiate(temp, instanceCoord, Quaternion.identity);
                    }

                    mapX++;

                    if (mapX % layerWidth == 0) {
                        //last tile in row
                        mapY++;
                        mapX = 0;
                    }
                }
            } else if (layerInfo.Attributes["name"].Value == "Interactive") {
                XmlNode tempNode = layerInfo.SelectSingleNode("data");

                int mapX, mapY;
                mapX = mapY = 0;

                foreach (XmlNode tile in tempNode.SelectNodes("tile")) {
                    int spriteValue = int.Parse(tile.Attributes["gid"].Value);

                    if (spriteValue > 0) {
                        GameObject temp = new GameObject(string.Format("tile {0} {1}", mapX, mapY));
                        SpriteRenderer renderer = temp.AddComponent<SpriteRenderer>();
                        renderer.sprite = sprites[spriteValue - 1];

                        float locationX = tileWidth * mapX;
                        float locationY = tileHeight * mapY;
                        Vector3 instanceCoord = new Vector3(locationX, locationY, 0f);

                        temp.transform.position = instanceCoord;

                        renderer.sortingLayerName = layerInfo.Attributes["name"].Value;

                        //Instantiate(temp, instanceCoord, Quaternion.identity);
                    }

                    mapX++;

                    if (mapX % layerWidth == 0) {
                        //last tile in row
                        mapY++;
                        mapX = 0;
                    }
                }
            }
        }
    }

    void Update() {}
}