﻿#pragma warning disable 0414, 0219

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: SpawnEnemy 
/// </summary>
public class SpawnEnemy : MonoBehaviour {
    #region Fields

    Rigidbody2D enemy;

    #endregion

    void EnemySpawn() {
        Rigidbody2D enemyInstance = Instantiate(Resources.Load("Prefabs/Enemy") as GameObject,
                                                new Vector3(Random.Range(2, 8), Random.Range(-4, 4), 0),
                                                Quaternion.identity) as Rigidbody2D;
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.name == "orb(Clone)") {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start() {
        InvokeRepeating("EnemySpawn", 3f, 3f);
    }

    // Update is called once per frame
    void Update() {}
}