﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: Controller 
/// </summary>
public class Controller : MonoBehaviour {
	#region Fields

    public Text scoreText;
    float score;
	#endregion

	// Use this for initialization
	void Start () {
	    scoreText = GameObject.Find("userScore").GetComponent<Text>();
        InvokeRepeating("AddTime", 0.5f, 0.5f);
    }

    void AddTime() {
        score += 0.5f;
        scoreText.text = System.Math.Round(score, 1).ToString();
    }
}
