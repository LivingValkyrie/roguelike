﻿#pragma warning disable 0414, 0219

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: enemyMovement 
/// </summary>
public class EnemyMovement : MonoBehaviour {
    #region Fields

    GameObject heroGameObject;
    bool enemyRight, enemyLeft, enemyUp, enemyDown;
    float enemySpeed;
    Animator enemyAnimator;

    #endregion

    // Use this for initialization
    void Start() {
        enemyDown = enemyRight = enemyLeft = enemyUp = false;
        enemyAnimator = GetComponent<Animator>();
        heroGameObject = GameObject.Find("Hero");
        enemySpeed = 1.0f;
        InvokeRepeating("Accelerate", 2f, 5f);
    }

    void Accelerate() {
        enemySpeed++;
    }

    void EnemyMove() {
        if (heroGameObject != null) {
            if (transform.position.y > heroGameObject.transform.position.y) {
                enemyAnimator.SetBool("Up", false);
                enemyAnimator.SetBool("Down", true);
                enemyAnimator.SetBool("Left", false);
                enemyAnimator.SetBool("Right", false);

                enemyDown = true;
                enemyUp = enemyLeft = enemyRight = false;

                transform.Translate(Vector3.down * enemySpeed * Time.deltaTime);
            } else {
                enemyAnimator.SetBool("Up", true);
                enemyAnimator.SetBool("Down", false);
                enemyAnimator.SetBool("Left", false);
                enemyAnimator.SetBool("Right", false);

                enemyUp = true;
                enemyDown = enemyLeft = enemyRight = false;

                transform.Translate(Vector3.up * enemySpeed * Time.deltaTime);
            }

            if (transform.position.x > heroGameObject.transform.position.x) {
                enemyAnimator.SetBool("Up", false);
                enemyAnimator.SetBool("Down", false);
                enemyAnimator.SetBool("Left", true);
                enemyAnimator.SetBool("Right", false);

                enemyLeft = true;
                enemyUp = enemyRight = enemyDown = false;

                transform.Translate(Vector3.left * enemySpeed * Time.deltaTime);
            } else {
                enemyAnimator.SetBool("Up", false);
                enemyAnimator.SetBool("Down", false);
                enemyAnimator.SetBool("Left", false);
                enemyAnimator.SetBool("Right", true);

                enemyRight = true;
                enemyDown = enemyLeft = enemyUp = false;

                transform.Translate(Vector3.right * enemySpeed * Time.deltaTime);
            }
        }
    }

    // Update is called once per frame
    void Update() {
        EnemyMove();
    }
}