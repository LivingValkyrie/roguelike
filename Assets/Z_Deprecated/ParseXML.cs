﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: ParseXML 
/// </summary>
public class ParseXML : MonoBehaviour {
    #region Fields

    XmlDocument products;
    TextAsset productDoc;

    public bool printMyWay = false;

    //3 types xmlDoc, xmltextReader, xmlReader
    //Ouput the Title/Artist if the CD year is after 1980
    //Output the Title/Artist/Country/Company if the CD year is after 1990
    //Don't output anything if CD year is 2000 or later

    #endregion

    void Start() {
        productDoc = Resources.Load("CDCatalog") as TextAsset;

        products = new XmlDocument();

        products.LoadXml(productDoc.text);

        if (printMyWay) {
            MyWay();
        } else {
            TiffanysWay();
        }
    }

    void TiffanysWay() {
        foreach (XmlNode node in products.DocumentElement.ChildNodes) {
            foreach (XmlNode childNode in node.ChildNodes) {
                if (childNode.Name == "YEAR") {
                    //checking year
                    //print("in year check " + Int32.Parse( childNode.InnerText ) );

                    string lineToPrint = "";

                    if (Int32.Parse(childNode.InnerText) >= 1980) {
                        foreach (XmlNode selectedNode in node.ChildNodes) {
                            if (selectedNode.Name == "TITLE") {
                                lineToPrint += "Title: " + selectedNode.InnerText;
                            } else if (selectedNode.Name == "ARTIST") {
                                lineToPrint += " Artist: " + selectedNode.InnerText;
                            }
                        }
                    }

                    if (Int32.Parse(childNode.InnerText) >= 1990) {
                        foreach (XmlNode selectedNode in node.ChildNodes) {
                            if (selectedNode.Name == "COUNTRY") {
                                lineToPrint += "Country: " + selectedNode.InnerText;
                            } else if (selectedNode.Name == "COMPANY") {
                                lineToPrint += " Company: " + selectedNode.InnerText;
                            }
                        }
                    }

                    if (Int32.Parse(childNode.InnerText) >= 2000) {
                        lineToPrint = "";
                    }

                    if (!String.IsNullOrEmpty(lineToPrint)) {
                        print(lineToPrint);
                    }
                }
            }
        }
    }

    void MyWay() {
        List<CD> catalog = new List<CD>();

        foreach (XmlNode CDNode in products.DocumentElement.ChildNodes) {
            string title = "";
            string artist = "";
            string country = "";
            string company = "";
            float price = 0f;
            int year = 0;

            foreach (XmlNode childNode in CDNode.ChildNodes) {
                switch (childNode.Name) {
                    case "TITLE":
                        title = childNode.InnerText;
                        break;
                    case "ARTIST":
                        artist = childNode.InnerText;
                        break;
                    case "COUNTRY":
                        country = childNode.InnerText;
                        break;
                    case "COMPANY":
                        company = childNode.InnerText;
                        break;
                    case "PRICE":
                        price = float.Parse(childNode.InnerText);
                        break;
                    case "YEAR":
                        year = Int32.Parse(childNode.InnerText);
                        break;
                }
            }

            //CD cd = new CD(title, artist, country, company, price, year);
            catalog.Add( new CD( title, artist, country, company, price, year ) );

        }

        foreach (CD cd in catalog) {
            if (cd.year >= 1980) {
                print(string.Format("Title: {0} Artist: {1}", cd.title, cd.artist));
            }else if (cd.year >= 1990) {
                print( string.Format( "Title: {0} Artist: {1} Country: {2} Company: {3}", cd.title, cd.artist, cd.country, cd.company ) );
            }
        }
    }
}

public struct CD {
   public readonly string title;
   public readonly string artist;
   public readonly string country;
   public readonly string company;
   public readonly float price;
   public readonly int year;

    public CD( string title, string artist, string country, string company, float price, int year ) {
        this.title = title;
        this.artist = artist;
        this.country = country;
        this.company = company;
        this.price = price;
        this.year = year;
    }

    public override string ToString() {
        return String.Format("{0}, {1}, {2}, {3}, {4}, {5}", title, artist, country, company, price, year);
    }
}