﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: OneFingerGesture 
/// </summary>
public class OneFingerGesture : MonoBehaviour {
    #region Fields

    public int comfort;

    GameObject objToMove;
    Vector2 previous;
    Vector2 current;
    float touchDelta;

    #endregion

    void Update() {
        if (Input.touchCount == 1) {
            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                previous = Input.GetTouch(0).position;
            } else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
                current = Input.GetTouch(0).position;
                touchDelta = current.magnitude - previous.magnitude;

                if (Mathf.Abs(touchDelta) > comfort) {
                    //print("swipe read");
                    if (touchDelta > 0) {
                        //print("left and bottom");
                        if ( Mathf.Abs( current.x - previous.x ) > Mathf.Abs( current.y - previous.y ) ) {
                            print( "left" );
                        } else {
                            print( "bottom" );
                        }
                    } else {
                        //print("top and right");
                        if (Mathf.Abs(current.x - previous.x) > Mathf.Abs(current.y - previous.y)) {
                            print("right");
                        } else {
                            print("top");
                        }
                    }
                } else {
                    HandleTaps();
                }
            }
        }
    }

    void HandleTaps() {
        if (Input.GetTouch(0).tapCount == 1) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit)) {
                if (objToMove != null && hit.transform != objToMove.transform) {
                    objToMove.GetComponent<Renderer>().material.color = Color.white;
                }

                objToMove = hit.transform.gameObject;
                objToMove.GetComponent<Renderer>().material.color = Color.blue;
            }
        } else if (Input.GetTouch(0).tapCount == 2) {
            if (objToMove != null) {
                float camToObjDist = Mathf.Abs(Camera.main.transform.position.z - objToMove.transform.position.z);
                Vector3 pos = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, camToObjDist);

                objToMove.transform.position = Camera.main.ScreenToWorldPoint(pos);
            }
        }
    }

}