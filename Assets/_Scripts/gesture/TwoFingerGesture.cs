﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: TwoFingerGesture 
/// </summary>
public class TwoFingerGesture : MonoBehaviour {
    #region Fields

    public GameObject objToRotate;
    Vector2 current, previous;
    float touchDelta;
    public float comfort;
    public Camera cam;
    public int zoomtime = 5;
    float angle;
    public int rotationSpeed = 8;

    #endregion

    void Update() {
        if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved) {
            current = Input.GetTouch(0).position - Input.GetTouch(1).position;
            previous = (Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) -
                       (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition);
            touchDelta = current.magnitude - previous.magnitude;
            angle = Vector2.Angle(previous, current);

            if (angle > 0.1f) {
                if (Vector3.Cross(current, previous).z < 0) {
                    objToRotate.transform.Rotate(Vector3.up, -angle * rotationSpeed);
                } else {
                    objToRotate.transform.Rotate(Vector3.up, angle * rotationSpeed);
                }
            }

            if (Mathf.Abs(touchDelta) > comfort) {
                if (touchDelta > 0) {
                    cam.fieldOfView = Mathf.Clamp(Mathf.Lerp(cam.fieldOfView,
                                                             cam.fieldOfView - Mathf.Abs(touchDelta),
                                                             Time.deltaTime * zoomtime),10,70);
                } else {
                    cam.fieldOfView = Mathf.Clamp(Mathf.Lerp(cam.fieldOfView,
                                                             cam.fieldOfView + Mathf.Abs(touchDelta),
                                                             Time.deltaTime * zoomtime),10,70);
                }
            }
        }
    }
}