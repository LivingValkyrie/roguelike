﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: LoadMap 
/// </summary>
public class LevelController : MonoBehaviour {
	#region Fields

	public GameObject[] maps;
	public Text countDown;

	public int score;

	#endregion

	void Start() {
		LoadMapToScene(Random.Range(0, maps.Length));
		score = 5000;
		GameOptions.s.ActivateSwipeZones();
	}

	void Update() {
		score--;
		countDown.text = score.ToString();

		if (score <= 0) {
			SceneManager.LoadScene("GameEnd");
		}
	}

	/// <summary>
	/// Loads the map to scene.
	/// </summary>
	public void LoadMapToScene(int index) {
		if (transform.childCount > 0) {
			for (int i = 0; i < transform.childCount; i++) {
				Destroy(transform.GetChild(i).gameObject);
			}
		}

		//load prefab by index, if index out of range load closest index?
		if (index < 0) {
			index = 0;
		} else if (index > maps.Length - 1) {
			index = maps.Length - 1;
		}

		GameObject map = Instantiate(maps[index]);
		map.transform.position = Vector3.zero;
		map.transform.parent = transform;

		//add 5k time
		score += 5000;
	}
}