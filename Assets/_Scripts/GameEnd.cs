﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: GameEnd 
/// </summary>
public class GameEnd : MonoBehaviour {
	#region Fields

	public UnityEngine.UI.Text gameOverText;

	#endregion

	void Start() {
		GameOptions.s.ActivateSwipeZones(false);
		if (GameOptions.s.playerHealth > 0) {
			if (GameOptions.s.maxLevel >= 3) {
				gameOverText.text = "You Won!" + "\n"
				                    + "Score: " + GameOptions.s.score + "\n"
				                    + "HIghest Level:" + GameOptions.s.maxLevel;
			}
		} else {
			gameOverText.text = "You Lost!" + "\n"
			                    + "Score: " + GameOptions.s.score + "\n"
			                    + "HIghest Level:" + GameOptions.s.maxLevel;
		}
	}

	public void LoadMenu() {
		SceneManager.LoadScene("Menu");
	}

	public void Replay() {

		PlayerPrefs.SetInt( GameOptions.levelString, 0 );
		PlayerPrefs.SetInt( GameOptions.healthString, 100 );
		SceneManager.LoadScene( "Game", LoadSceneMode.Single );
	}
}