﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: GameOptions 
/// </summary>
public class GameOptions : MonoBehaviour {
	#region Fields

	public static GameOptions s;

	[HideInInspector]
	public bool playMusic;

	[HideInInspector]
	public bool playSoundEffects;

	[HideInInspector]
	public Difficulty difficulty;

	[HideInInspector]
	public bool useAccelerometer;

	[HideInInspector]
	public static string levelString = "Max Level";

	[HideInInspector]
	public static string healthString = "Health";

	[HideInInspector]
	public GameObject[] enemiePrefabs;

	[HideInInspector]
	public List<EnemySpawner> spawners;

	[HideInInspector]
	public List<GameObject> enemies;

	[HideInInspector]
	public int maxLevel;

	[HideInInspector]
	public int playerHealth;

	[HideInInspector]
	public int score;

	[HideInInspector]
	public GameObject swipeZoneParent;

	#endregion

	void Start() {
		if (s != null) {
			Destroy(this.gameObject);
		} else {
			s = this;

			playMusic = true;
			playSoundEffects = true;
			difficulty = Difficulty.Easy;
			useAccelerometer = false;

			enemiePrefabs = Resources.LoadAll<GameObject>("Prefabs/Enemies");
			swipeZoneParent = transform.GetChild(0).gameObject;
			swipeZoneParent.SetActive(false);
		}

		DontDestroyOnLoad(this.gameObject);

		if (!PlayerPrefs.HasKey(levelString)) {
			PlayerPrefs.SetInt(levelString, 0);
			PlayerPrefs.SetInt(healthString, 100);
		}
	}

	public void SpawnEnemies() {
		for (int i = 0; i < spawners.Count; i++) {
			GameObject temp;
			if (i == 0) {
				temp = Instantiate(enemiePrefabs[i]);
				temp.GetComponent<Enemy>().type = EnemyType.Boss;
			} else {
				int index = Random.Range(1, enemiePrefabs.Length);
				temp = Instantiate(enemiePrefabs[index]);
				temp.GetComponent<Enemy>().type = (EnemyType) index;
			}

			temp.transform.position = spawners[i].transform.position;

			enemies.Add(temp);
		}
	}

	public void StartSpawnCounter() {
		CancelInvoke("SpawnEnemies");
		Invoke("SpawnEnemies", 5f);
	}

	public void ActivateSwipeZones(bool active = true) {
		swipeZoneParent.SetActive(active);
	}

	public void ToggleMusic(bool value) {
		s.playMusic = value;
	}

	public void ToggleSoundEffects(bool value) {
		s.playSoundEffects = value;
	}

	public void ToggleAccelerometer(bool value) {
		s.useAccelerometer = value;
	}

	public void ChangeDifficulty(int value) {
		s.difficulty = (Difficulty) Mathf.Clamp(value, 0, 2);

		//Debug.Log(s.difficulty);
	}
}

public enum Difficulty {
	Easy,
	Medium,
	Hard
}