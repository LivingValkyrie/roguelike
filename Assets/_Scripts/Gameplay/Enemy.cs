﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: Enemy 
/// </summary>
[RequireComponent(typeof (AudioSource))]
public class Enemy : MonoBehaviour {
	#region Fields

	public EnemyType type;
	public EnemyStats stats;
	bool isAlive;

	#endregion

	void Start() {
		stats = EnemyStats.GetStatsByType(type);
		isAlive = true;

		switch (type) {
			case EnemyType.Boss:
				break;
			case EnemyType.Soldier:
				break;
			case EnemyType.Soldier2:
				break;
			case EnemyType.Soldier3:
				break;
		}
	}

	void Update() {
		switch (type) {
			case EnemyType.Boss:
				break;
			case EnemyType.Soldier:
				break;
			case EnemyType.Soldier2:
				break;
			case EnemyType.Soldier3:
				break;
		}

		if (stats.health <= 0 && isAlive) {
			isAlive = false;
			Death(true);
		}
	}

	void Death(bool playSound) {
		if (playSound) {
			GetComponent<AudioSource>().Play();
		}

		if (type == EnemyType.Boss) {
			Progress();

			GameOptions.s.enemies.Remove(gameObject);

			while (GameOptions.s.enemies.Count > 0) {
				GameOptions.s.enemies[0].GetComponent<Enemy>().Death(false);
			}
		} else {
			GameOptions.s.enemies.Remove(gameObject);
			foreach (GameObject enemy in GameOptions.s.enemies) {
				enemy.GetComponent<Enemy>().ScaleUp();
			}
		}

		//being called to soon for sound to play
		Destroy(gameObject, GetComponent<AudioSource>().clip.length);
	}

	public void ScaleUp() {
		switch (type) {
			case EnemyType.Boss:
				stats += EnemyStats.BoostLarge;
				break;
			case EnemyType.Soldier:
			case EnemyType.Soldier2:
			case EnemyType.Soldier3:
				stats += EnemyStats.DifficultyBasedBoost(GameOptions.s.difficulty);
				break;
		}
	}

	public void Progress() {
		LevelController ctrl = GameObject.Find("LevelController").GetComponent<LevelController>();

		GameOptions.s.maxLevel++;
		GameOptions.s.playerHealth = GameObject.Find("Hero").GetComponent<HeroMovement>().health;
		GameOptions.s.score = ctrl.score;
		GameOptions.s.spawners = new List<EnemySpawner>();
		if (GameOptions.s.maxLevel >= 3) {
			SceneManager.LoadScene("GameEnd");
		} else {
			ctrl.LoadMapToScene(Random.Range(0, ctrl.maps.Length));
		}
	}
}

public enum EnemyType {
	Boss,
	Soldier,
	Soldier2,
	Soldier3
}

[System.Serializable]
public struct EnemyStats {

	public static EnemyStats Boss = new EnemyStats(100, 20, 1); //boss
	public static EnemyStats Soldier = new EnemyStats(30, 5, 1); //idle
	public static EnemyStats Soldier2 = new EnemyStats(60, 10, 1); // caster
	public static EnemyStats Soldier3 = new EnemyStats(40, 10, 1); //boomer, blows up when close for dmg

	public static EnemyStats BoostSmall = new EnemyStats(10, 2, 0);
	public static EnemyStats BoostMed = new EnemyStats(20, 5, 0);
	public static EnemyStats BoostLarge = new EnemyStats(30, 10, 0);

	public int health;
	public int attack;
	public float speed;

	public EnemyStats(int health, int attack, float speed) {
		this.health = health;
		this.attack = attack;
		this.speed = speed;
	}

	public static EnemyStats GetStatsByType(EnemyType type) {
		switch (type) {
			case EnemyType.Boss:
				return Boss;
			case EnemyType.Soldier:
				return Soldier;
			case EnemyType.Soldier2:
				return Soldier2;
			case EnemyType.Soldier3:
				return Soldier3;
			default:
				return Soldier;
		}
	}

	public static EnemyStats DifficultyBasedBoost(Difficulty diff) {
		switch (diff) {
			case Difficulty.Easy:
				return BoostSmall;
			case Difficulty.Medium:
				return BoostMed;
			case Difficulty.Hard:
				return BoostLarge;
			default:
				return BoostSmall;
		}
	}

	public static EnemyStats operator +(EnemyStats es1, EnemyStats es2) {
		return new EnemyStats(es1.health + es2.health, es1.attack + es2.attack, es1.speed + es2.speed);
	}
}