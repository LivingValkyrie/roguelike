﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: HeroMovement 
/// </summary>
public class HeroMovement : MonoBehaviour {
	#region Fields

	public float moveSpeed = 1;
	public float mobileMoveSpeed = 10;
	public int damage = 40;
	public int defense = 5;
	public int health = 100;

	public int MeleeDmg {
		get { return damage / 4; }
	}

	bool left = false, right = false, up = false, down = true;
	Animator animator;

	GameObject orb;
	GameObject slash;
	float orbSpeed = 20f;

	public int manaMax = 24, manaCost = 8;
	public int mana;
	Vector3 lastPos;

	public int comfort = 20;
	Vector2 previous;
	Vector2 current;
	float touchDelta;

	#endregion

	// Use this for initialization
	void Start() {
		animator = GetComponent<Animator>();
		orb = Resources.Load("Prefabs/Orb") as GameObject;
		slash = Resources.Load("Prefabs/Slash") as GameObject;

		mana = 8;
		lastPos = transform.position;

		Debug.LogWarning("Update player script for hud");
	}

	void Update() {
		mana = Mathf.Clamp(mana, 0, manaMax);

		if (health <= 0) {
			SceneManager.LoadScene("GameEnd");
		}

		if (Input.GetMouseButtonDown(0) && Input.touchCount <= 0) {
			SpawnOrb();
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			Melee();
		}

#if UNITY_ANDROID

		if (Input.touchCount <= 0) {
			return;
		}

		if (Input.touchCount == 1) {
			//check if raycast hit shoot zone

			if (Input.GetTouch(0).phase == TouchPhase.Began) {
				previous = Input.GetTouch(0).position;
			} else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
				current = Input.GetTouch(0).position;
				touchDelta = current.magnitude - previous.magnitude;

				if (Mathf.Abs(touchDelta) > comfort) {
					#region shoot

					RaycastHit2D hit = Physics2D.Raycast(previous, Vector2.zero);
					if (hit.transform.name == "cast") {
						print("swipe read on cast");
						if (touchDelta > 0) {
							//print("left and bottom");
							if (Mathf.Abs(current.x - previous.x) > Mathf.Abs(current.y - previous.y)) {
								TurnAndShoot("left");
							} else {
								TurnAndShoot("down");
							}
						} else {
							//print("top and right");
							if (Mathf.Abs(current.x - previous.x) > Mathf.Abs(current.y - previous.y)) {
								TurnAndShoot("right");
							} else {
								TurnAndShoot("up");
							}
						}
					}

					#endregion
				} else {
					RaycastHit2D hit = Physics2D.Raycast(previous, Vector2.zero);
					if (hit.transform.name == "cast") {
						Melee();
					}
				}
			}
		}
#endif
	}

	void TurnAndShoot(string dir) {
		Debug.Log(dir);
		switch (dir) {
			case "right":
				animator.SetBool("Left", true);
				animator.SetBool("Right", false);
				animator.SetBool("Up", false);
				animator.SetBool("Down", false);

				right = up = down = false;
				left = true;
				break;
			case "left":
				animator.SetBool("Left", false);
				animator.SetBool("Right", true);
				animator.SetBool("Up", false);
				animator.SetBool("Down", false);

				left = up = down = false;
				right = true;
				break;
			case "down":
				animator.SetBool("Left", false);
				animator.SetBool("Right", false);
				animator.SetBool("Down", false);
				animator.SetBool("Up", true);

				//fix for weird Buge. couldnt find root cause
				animator.Play( "Walk" );

				right = left = down = false;
				up = true;
				break;
			case "up":
				animator.SetBool("Left", false);
				animator.SetBool("Right", false);
				animator.SetBool("Up", false);
				animator.SetBool("Down", true);

				right = up = left = false;
				down = true;
				break;
			default:
				break;
		}

		SpawnOrb();
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.name == "Enemy(Clone)") {
			Time.timeScale = 0f;
			Destroy(gameObject);
		}
	}

	void Melee() {
		Vector3 slashPos = transform.position;
		GameObject temp;
		float margin = 0.2f;

		if (right) {
			slashPos += Vector3.right * margin;
			temp = Instantiate(slash, slashPos, Quaternion.identity) as GameObject;
			temp.transform.parent = gameObject.transform;
		}
		if (left) {
			slashPos += Vector3.left * margin;
			temp = Instantiate(slash, slashPos, Quaternion.identity) as GameObject;
			temp.transform.localRotation = Quaternion.Euler(0, 180, 0);
			temp.transform.parent = gameObject.transform;
		}
		if (up) {
			slashPos += Vector3.up * margin;
			temp = Instantiate(slash, slashPos, Quaternion.identity) as GameObject;
			temp.transform.localRotation = Quaternion.Euler(0, 180, 0);
			temp.transform.parent = gameObject.transform;
		}
		if (down) {
			slashPos += Vector3.down * margin;
			temp = Instantiate(slash, slashPos, Quaternion.identity) as GameObject;
			temp.transform.parent = gameObject.transform;
		}
	}

	void SpawnOrb() {
		if (mana >= manaCost) {
			mana -= manaCost;

			GameObject orbInstance = Instantiate(orb, transform.position, Quaternion.identity) as GameObject;
			//orbInstance.transform.parent = transform;
			Rigidbody2D orbRigid = orbInstance.GetComponent<Rigidbody2D>();

			if (right) {
				orbRigid.velocity = new Vector2(orbSpeed, 0f);
			}
			if (left) {
				orbRigid.velocity = new Vector2(-orbSpeed, 0f);
			}
			if (up) {
				orbRigid.velocity = new Vector2(0f, orbSpeed);
			}
			if (down) {
				orbRigid.velocity = new Vector2(0f, -orbSpeed);
			}
		}
	}

	// Update is called once per frame
	void FixedUpdate() {
		MoveCharacterPc();
#if UNITY_ANDROID
		if (Input.touchCount <= 0) {
			return;
		}

		if (Input.touchCount == 1) {
			if (Input.GetTouch(0).phase == TouchPhase.Began) {
				previous = Input.GetTouch(0).position;
			} else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
				current = Input.GetTouch(0).position;
				touchDelta = current.magnitude - previous.magnitude;

				if (Mathf.Abs(touchDelta) > comfort) {
					RaycastHit2D hit = Physics2D.Raycast(previous, Vector2.zero);
					if (hit.transform.name == "move") {
						print("swipe read on move");
						if (touchDelta > 0) {
							//print("left and bottom");
							if (Mathf.Abs(current.x - previous.x) > Mathf.Abs(current.y - previous.y)) {
								MoveCharacterMobile("left");
							} else {
								MoveCharacterMobile("up");
							}
						} else {
							//print("top and right");
							if (Mathf.Abs(current.x - previous.x) > Mathf.Abs(current.y - previous.y)) {
								MoveCharacterMobile("right");
							} else {
								MoveCharacterMobile("down");
							}
						}
					}
				}
			}
		}

#endif
	}

	void MoveCharacterMobile(string dir) {
		switch (dir) {
			case "right":
				animator.SetBool("Left", true);
				animator.SetBool("Right", false);
				animator.SetBool("Up", false);
				animator.SetBool("Down", false);

				right = up = down = false;
				left = true;

				transform.Translate(Vector3.left * mobileMoveSpeed * Time.deltaTime);
				break;
			case "left":
				animator.SetBool("Left", false);
				animator.SetBool("Right", true);
				animator.SetBool("Up", false);
				animator.SetBool("Down", false);

				left = up = down = false;
				right = true;

				transform.Translate(Vector3.right * mobileMoveSpeed * Time.deltaTime);
				break;
			case "up":
				animator.SetBool("Left", false);
				animator.SetBool("Right", false);
				animator.SetBool("Down", false);
				animator.SetBool("Up", true);

				//fix for weird Buge. couldnt find root cause
				//animator.Play( "WalkUp" );

				right = left = down = false;
				up = true;

				transform.Translate(Vector3.up * mobileMoveSpeed * Time.deltaTime);
				break;
			case "down":
				animator.SetBool("Left", false);
				animator.SetBool("Right", false);
				animator.SetBool("Up", false);
				animator.SetBool("Down", true);

				right = up = left = false;
				down = true;

				transform.Translate(Vector3.down * mobileMoveSpeed * Time.deltaTime);
				break;
			default:
				break;
		}
	}

	void MoveCharacterPc() {
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			animator.SetBool("Left", false);
			animator.SetBool("Right", true);
			animator.SetBool("Up", false);
			animator.SetBool("Down", false);

			left = up = down = false;
			right = true;

			transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
			animator.SetBool("Left", true);
			animator.SetBool("Right", false);
			animator.SetBool("Up", false);
			animator.SetBool("Down", false);

			right = up = down = false;
			left = true;
			transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
			animator.SetBool("Left", false);
			animator.SetBool("Right", false);
			animator.SetBool("Down", false);
			animator.SetBool("Up", true);

			//fix for weird Buge. couldnt find root cause
			animator.Play("WalkUp");

			right = left = down = false;
			up = true;
			transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
			animator.SetBool("Left", false);
			animator.SetBool("Right", false);
			animator.SetBool("Up", false);
			animator.SetBool("Down", true);

			right = up = left = false;
			down = true;

			transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
		}

		if (Mathf.Abs((transform.position - lastPos).magnitude) > 0.5f) {
			mana = Mathf.Clamp(mana + 1, 0, manaMax);
			lastPos = transform.position;

			//Debug.Log(mana);
		}
	}

}