﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: Slash 
/// </summary>
public class Slash : MonoBehaviour {
	#region Fields

	int frameCount = 0;

	#endregion

	void Start() {}

	void FixedUpdate() {
		frameCount++;
		if (frameCount >= 24) {
			Destroy(gameObject);
		}
	}

	public void OnTriggerEnter2D(Collider2D other) {
		Enemy temp = other.GetComponent<Enemy>();
		if (temp) {
			temp.stats.health -= transform.parent.GetComponent<HeroMovement>().MeleeDmg;

			//Debug.Log("i hit an enemy");
		}
	}
}