﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: MoveOrb 
/// </summary>
[RequireComponent(typeof (AudioSource))]
public class MoveOrb : MonoBehaviour {
	#region Fields

	Rigidbody2D orb;

	public float lifespan = 4f;

	#endregion

	void Update() {
		if (lifespan <= 0) {
			Destroy(gameObject);
		}

		lifespan -= Time.deltaTime;
	}

	void OnTriggerEnter2D(Collider2D other) {
		Enemy temp = other.GetComponent<Enemy>();
		if (temp) {
			temp.stats.health -= FindObjectOfType<HeroMovement>().damage;

			//Debug.Log("i hit an enemy");
		}
	}
}