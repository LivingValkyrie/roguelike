﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: TileMap 
/// </summary>
public class TileMap : MonoBehaviour {
	#region Fields

	[Tooltip("The name of the folder in which all map tilesets are stored. Base directory is \"Assets/Resources/\"." +
	         " Do not include ending forward slash.")]
	public string tilesetDirectory = "Maps";

	[Tooltip("Must be a Tiled xml map.")]
	public TextAsset mapFile;

	[Tooltip("Adds components to the tiles if they have properties of the same name in Tiled.")]
	public bool addTileComponents = true;

	[Tooltip("Adds components to the tiles of each layer if the layer has properties of the same name in Tiled.")]
	public bool addLayerComponents = true;

	// public string spriteSheet;
	[HideInInspector]
	public Sprite[] sprites;

	public float PixelsPerUnit {
		get {
			if (sprites[0] != null) {
				return sprites[0].pixelsPerUnit;
			} else {
				return 1;
			}
		}
	}

	public List<Tile> specialTiles;
	public List<Layer> specialLayers;

	#endregion

	public void InitializeLayerCollection() {
		specialLayers = new List<Layer>();
	}

	public void InitializeSpecialLayers(XmlNode mapInfo) {
		if (specialLayers == null) {
			InitializeLayerCollection();
		}

		XmlNodeList tilesetChildren = mapInfo.ChildNodes;
		foreach (XmlNode child in tilesetChildren) {
			if (child.Name == "layer") {
				//print(child.Attributes["name"].Value);

				//get properties node and list
				XmlNode propertiesNode = child.SelectSingleNode("properties");

				//esc iteration if layer does not have special properties
				if (propertiesNode == null) {
					continue;
				}

				//each property node
				XmlNodeList propertiesNodeList = propertiesNode.ChildNodes;

				//temp array
				LayerProperty[] tempProperties = new LayerProperty[propertiesNodeList.Count];
				for (int i = 0; i < propertiesNodeList.Count; i++) {
					tempProperties[i] = new LayerProperty(propertiesNodeList[i].Attributes["name"].Value,
					                                      propertiesNodeList[i].Attributes["value"].Value);

					//Debug.Log(child.Attributes["name"].Value + " is getting " + propertiesNodeList[i].Attributes["name"].Value );
				}

				//create layer object
				Layer temp = new Layer(child.Attributes["name"].Value, tempProperties);

				//fail safe for editor script
				foreach (Layer specialLayer in specialLayers) {
					if (temp.name == specialLayer.name) {
						temp = null;
						break;
					}
				}

				//fail safe part 2
				if (temp != null) {
					specialLayers.Add(temp);
				}
			}
		}
	}

	#region special tiles

	public void InitializeTileCollection() {
		specialTiles = new List<Tile>();
	}

	public void InitialiseSpecialTiles(XmlNode tilesetInfo) {
		if (specialTiles == null) {
			InitializeTileCollection();
		}

		XmlNodeList tilesetChildren = tilesetInfo.ChildNodes;
		foreach (XmlNode child in tilesetChildren) {
			if (child.Name == "tile") {
				//need to increase by 1 for use within drawing and dictionary key
				int tileKey = int.Parse(child.Attributes["id"].Value);

				//get properties node and list
				XmlNode propertiesNode = child.SelectSingleNode("properties");
				XmlNodeList propertiesNodeList = propertiesNode.ChildNodes;

				TileProperty[] tempProperties = new TileProperty[propertiesNodeList.Count];
				for (int i = 0; i < propertiesNodeList.Count; i++) {
					//create property for this node
					tempProperties[i] = new TileProperty(propertiesNodeList[i].Attributes["name"].Value,
					                                     propertiesNodeList[i].Attributes["value"].Value);
				}

				//create tile object
				Tile temp = new Tile(tileKey, "Tile id: " + tileKey, tempProperties, sprites[tileKey]);

				//fail safe for editor script
				foreach (Tile specialTile in specialTiles) {
					if (temp.id == specialTile.id) {
						temp = null;
						break;
					}
				}

				//fail safe prt 2
				if (temp != null) {
					specialTiles.Add(temp);
				}
			}
		}
	}

	#endregion

	public void GenerateMap() {
		if (specialTiles == null) {
			InitializeTileCollection();
		}

		//create doc object and give it map info
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(mapFile.text);

		//get map node, holds all info
		XmlNode mapInfo = xmlDoc.SelectSingleNode("map");

		//get all layer nodes
		XmlNodeList layerNames = xmlDoc.GetElementsByTagName("layer");

		//get tileset info
		XmlNode tilesetInfo = mapInfo.SelectSingleNode("tileset");

		//get properties. will need to change if i do individual bools
		InitialiseSpecialTiles(tilesetInfo);
		InitializeSpecialLayers(mapInfo);

		sprites = Resources.LoadAll<Sprite>(tilesetDirectory + "/" + tilesetInfo.Attributes["name"].Value);

		float tileWidth = int.Parse(mapInfo.Attributes["tilewidth"].Value) / PixelsPerUnit;
		float tileHeight = int.Parse(mapInfo.Attributes["tileheight"].Value) / PixelsPerUnit;

		GameObject mapParent = new GameObject();
		mapParent.transform.position = Vector3.zero;
		mapParent.name = mapFile.name;

		//each layer
		foreach (XmlNode layerInfo in layerNames) {
			int layerWidth = int.Parse(layerInfo.Attributes["width"].Value);
			int layerHeight = int.Parse(layerInfo.Attributes["height"].Value);

			//data node, holds tile nodes (not layer.... grumble grumble)
			XmlNode dataNode = layerInfo.SelectSingleNode("data");

			int mapX = 0;
			int mapY = layerHeight - 1;
			string layerName = layerInfo.Attributes["name"].Value;

			//print(layerName);

			if (!LVGUnityWrappersAndExtensions.GetSortingLayers().Contains(layerName)) {
				LVGUnityWrappersAndExtensions.AddSortingLayer();

				//change newest layer name to that of layerName
				int index = LVGUnityWrappersAndExtensions.GetSortingLayerCount() - 1;

				LVGUnityWrappersAndExtensions.SetSortingLayerName(index, layerName);
			}

			GameObject layerParent = new GameObject();
			layerParent.transform.position = Vector3.zero;
			layerParent.name = layerName;
			layerParent.transform.parent = mapParent.transform;

			//set up properties for tile

			foreach (XmlNode tile in dataNode.SelectNodes("tile")) {
				int spriteValue = int.Parse(tile.Attributes["gid"].Value);

				if (spriteValue > 0) {
					GameObject tileObj = new GameObject(string.Format("{2} tile {0} {1}", mapX, mapY, layerName));
					SpriteRenderer renderer = tileObj.AddComponent<SpriteRenderer>();
					renderer.sprite = sprites[spriteValue - 1];

					float locationX = tileWidth * mapX;
					float locationY = tileHeight * mapY;
					Vector3 instanceCoord = new Vector3(locationX, locationY, 0f);

					tileObj.transform.position = instanceCoord;

					renderer.sortingLayerName = layerName;
					tileObj.transform.parent = layerParent.transform;

					//add components for special tiles
					if (addTileComponents) {
						foreach (Tile specialTile in specialTiles) {
							if (specialTile.id == spriteValue - 1) {
								foreach (TileProperty property in specialTile.properties) {
									Type componentType = Type.GetType(property.name);
									Type unityType = Type.GetType("UnityEngine." + property.name + ",UnityEngine");

									if (property.addAsComponent) {
										if (componentType != null) {
											tileObj.AddComponent(componentType);
											//Debug.Log(property.name + " has been added");
										} else {
											if (unityType != null) {
												tileObj.AddComponent(unityType);
												//Debug.Log(property.name + " has been added");
											} else {
												Debug.LogWarning("The component \"" + property.name + "\" does not exist. Is the spelling correct?");
											}
										}
									} else {
										//Debug.Log(property.name + " is false");
									}
								}
							}
						}
					}

					//add layer components
					if (addLayerComponents) {
						foreach (Layer specialLayer in specialLayers) {
							if (specialLayer.name == layerName) {
								foreach (LayerProperty property in specialLayer.properties) {
									Type componentType = Type.GetType(property.name);
									Type unityType = Type.GetType("UnityEngine." + property.name + ",UnityEngine");
									
									if (property.addAsComponent) {
										if (componentType != null) {
											//test if tile already has this component
											if (tileObj.GetComponent(componentType) == null) {
												tileObj.AddComponent(componentType);

												Debug.Log(property.name + " has been added");
											}
										} else {
											if (unityType != null) {
												if (tileObj.GetComponent(unityType) == null) {
													tileObj.AddComponent(unityType);
													Debug.Log(property.name + " has been added");
												} else {
													Debug.Log("component already on obj " + property.name);

													Debug.Log("component is: "+ tileObj.GetComponent(unityType).GetType());
												}
											} else {
												Debug.LogWarning("The component \"" + property.name + "\" does not exist. Is the spelling correct?");
											}
										}
									} else {
										Debug.Log(property.name + " set to false");
									}
								}
							}
						}
					}
				}

				mapX++;

				if (mapX % layerWidth == 0) {
					//last tile in row
					mapY--;
					mapX = 0;
				}
			}
		}
	}

}

public class Tile {

	public string name;
	public TileProperty[] properties;
	public int id; // the same value as in sprites array
	public Sprite image;

	public Tile(int id, string name, int propertyCount) {
		this.id = id;
		this.name = name;
		properties = new TileProperty[propertyCount];
	}

	public Tile(int id, string name, TileProperty[] properties) {
		this.id = id;
		this.name = name;
		this.properties = properties;
	}

	public Tile(int id, string name, TileProperty[] properties, Sprite image) {
		this.id = id;
		this.name = name;
		this.properties = properties;
		this.image = image;
	}

}

public struct TileProperty {
	public string name;
	public string value;
	public bool addAsComponent;

	public TileProperty(string name, string value, bool addAsComponent) {
		this.name = name;
		this.value = value;
		this.addAsComponent = addAsComponent;
	}

	public TileProperty(string name, string value) {
		this.name = name;
		this.value = value;
		addAsComponent = true;
	}
}

public class Layer {
	public string name;
	public LayerProperty[] properties;

	public Layer(string name, int propertyCount) {
		this.name = name;
		properties = new LayerProperty[propertyCount];
	}

	public Layer(string name, LayerProperty[] properties) {
		this.name = name;
		this.properties = properties;
	}
}

public struct LayerProperty {
	public string name;
	public string value;
	public bool addAsComponent;

	public LayerProperty(string name, string value, bool addAsComponent) {
		this.name = name;
		this.value = value;
		this.addAsComponent = addAsComponent;
	}

	public LayerProperty(string name, string value) {
		this.name = name;
		this.value = value;
		addAsComponent = true;
	}
}