﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: TileMapEditor 
/// </summary>
[CustomEditor(typeof (TileMap))]
public class TileMapEditor : Editor {
	#region Fields

	bool showLayers = false;
	bool showTiles = false;
	bool[] showTileProperties;
	bool[] showLayerProperties;
	float test;

	#endregion

	public void OnEnable() {}

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		TileMap myTarget = (TileMap) target;

		if (myTarget.mapFile != null) {
			//map already set
			//get xml info for layer names
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(myTarget.mapFile.text);

			//get map node, holds all info
			XmlNode mapInfo = xmlDoc.SelectSingleNode("map");

			//get tileset info
			XmlNode tilesetInfo = mapInfo.SelectSingleNode("tileset");

			myTarget.sprites = Resources.LoadAll<Sprite>( myTarget.tilesetDirectory + "/" + tilesetInfo.Attributes["name"].Value );

			EditorGUILayout.Space();
			
			#region layer properties gui

			//layer foldout
			showLayers = EditorGUILayout.Foldout(showLayers, "Show Layers information");

			//show fold out info
			if (showLayers) {
				myTarget.InitializeSpecialLayers(mapInfo);

				//new array if needed
				if (showLayerProperties == null || showLayerProperties.Length != myTarget.specialLayers.Count) {
					showLayerProperties = new bool[myTarget.specialLayers.Count];
				}

				//iterate thru arrays
				for (int i = 0; i < myTarget.specialLayers.Count; i++) {
					EditorGUI.indentLevel++;

					string layerName = myTarget.specialLayers[i].name;

					showLayerProperties[i] = EditorGUILayout.Foldout(showLayerProperties[i], layerName);
					if (showLayerProperties[i]) {
						//get properties for special layer with matching name
						//foreach (Layer specialLayer in myTarget.specialLayers) {
						//	if (specialLayer.name == name) {
						//		EditorGUILayout.LabelField("names match");
						//		foreach (LayerProperty layerProperty in specialLayer.properties) {
						//			EditorGUILayout.LabelField(layerProperty.name);
						//		}
						//	}
						//}

						for (int j = 0; j < myTarget.specialLayers[i].properties.Length; j++) {
							EditorGUILayout.BeginHorizontal( new GUILayoutOption[0] );

							EditorGUILayout.LabelField(myTarget.specialLayers[i].properties[j].name); //property name

							EditorGUILayout.Space();

							myTarget.specialLayers[i].properties[j].addAsComponent = EditorGUILayout.ToggleLeft( "Add as component?",
																											   myTarget.specialLayers[i].properties[j].addAsComponent,
																											   new GUILayoutOption[0] );

							EditorGUILayout.EndHorizontal();
						}
					}

					EditorGUI.indentLevel--;
				}
			}

			#endregion

			EditorGUILayout.Space();

			#region tile properties gui

			//get properties
			showTiles = EditorGUILayout.Foldout(showTiles, "Show Tiles information");
			if (showTiles) {
				myTarget.InitialiseSpecialTiles(tilesetInfo);

				if (showTileProperties == null || showTileProperties.Length != myTarget.specialTiles.Count) {
					showTileProperties = new bool[myTarget.specialTiles.Count];

					//Debug.Log(showTileProperties.Length + " " + myTarget.specialTiles.Count);
				}

				for (int i = 0; i < myTarget.specialTiles.Count; i++) {
					EditorGUI.indentLevel++;
					EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);

					Texture2D tileTexture = AssetPreview.GetAssetPreview( myTarget.specialTiles[i].image );
					GUILayout.Label( tileTexture, new GUILayoutOption[] {GUILayout.Width(25), GUILayout.Height(25)} );

					showTileProperties[i] = EditorGUILayout.Foldout(showTileProperties[i],
					                                                "show " + myTarget.specialTiles[i].name + " properties");

					EditorGUILayout.EndHorizontal();

					if (showTileProperties[i]) {
						//this is each property of the tile
						for (int j = 0; j < myTarget.specialTiles[i].properties.Length; j++) {
							EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);

							EditorGUILayout.LabelField(myTarget.specialTiles[i].properties[j].name); //property name

							EditorGUILayout.Space();
							myTarget.specialTiles[i].properties[j].addAsComponent = EditorGUILayout.ToggleLeft("Add as component?",
							                                                                                   myTarget.specialTiles[i].properties[j].addAsComponent,
							                                                                                   new GUILayoutOption[0]);

							EditorGUILayout.EndHorizontal();
						}
							EditorGUILayout.Space();
					}

					EditorGUI.indentLevel--;
				}
			}

			#endregion

			EditorGUILayout.Space();

			if (GUILayout.Button("Generate Layers")) {
				//Debug.Log("button press accepted");

				myTarget.GenerateMap();
			}
		}
	}

	void CreateSortingLayer() {
		Debug.Log("initial score: " + InternalEditorUtility.layers.Count());
		InternalEditorUtility internalEditorUtility = new InternalEditorUtility();

		System.Type internalEditorUtilityType = typeof (InternalEditorUtility);
		PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames",
		                                                                           BindingFlags.Static | BindingFlags.NonPublic);
		string[] sortingLayerNamess = (string[]) sortingLayersProperty.GetValue(null, new object[0]);

		foreach (string s in sortingLayerNamess) {
			Debug.Log(s);
		}

		//sortingLayersProperty.SetValue();

		MethodInfo addLayerMethod = internalEditorUtility.GetType()
		                                                 .GetMethod("AddSortingLayer",
		                                                            BindingFlags.NonPublic | BindingFlags.Static);
		MethodInfo getSortingLayerCountMethod = internalEditorUtility.GetType()
		                                                             .GetMethod("GetSortingLayerCount",
		                                                                        BindingFlags.NonPublic | BindingFlags.Static);

		addLayerMethod.Invoke(this, new object[] {});
		Debug.Log("Sorting Layer Count: " + getSortingLayerCountMethod.Invoke(this, new object[] {}));
	}
}