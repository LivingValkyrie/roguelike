using System.Collections.Generic;
using System.Reflection;
using UnityEditorInternal;
using UnityEngine.UI;

public class LVGUnityWrappersAndExtensions {

    InternalEditorUtility internalEditorUtility = new InternalEditorUtility();
    static System.Type internalEditorUtilityType = typeof (InternalEditorUtility);
    static MethodInfo addSortingLayerMethod;
    static MethodInfo getSortingLayerCountMethod;
    static MethodInfo setSortingLayerName;

    static PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames",
                                                                                      BindingFlags.Static | BindingFlags.NonPublic);

    public static string[] sortingLayerNames = (string[]) sortingLayersProperty.GetValue(null, new object[0]);

    static LVGUnityWrappersAndExtensions s = new LVGUnityWrappersAndExtensions();

    public LVGUnityWrappersAndExtensions() {
        addSortingLayerMethod = internalEditorUtility.GetType().GetMethod("AddSortingLayer",
                                                                          BindingFlags.NonPublic | BindingFlags.Static);

        getSortingLayerCountMethod = internalEditorUtility.GetType().GetMethod("GetSortingLayerCount",
                                                                               BindingFlags.NonPublic | BindingFlags.Static);

        setSortingLayerName = internalEditorUtility.GetType().GetMethod("SetSortingLayerName",
                                                                        BindingFlags.NonPublic | BindingFlags.Static);
    }

    public static void AddSortingLayer() {
        addSortingLayerMethod.Invoke(s, new object[] {});
    }

    public static int GetSortingLayerCount() {
        return (int) getSortingLayerCountMethod.Invoke(s, new object[] {});
    }

    public static void SetSortingLayerName(int index, string name) {
        setSortingLayerName.Invoke(s, new object[] {index, name});
    }

    public static string[] GetSortingLayers() {
        PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames",
                                                                                   BindingFlags.Static | BindingFlags.NonPublic);

        return (string[]) sortingLayersProperty.GetValue(null, new object[0]);
    }
}