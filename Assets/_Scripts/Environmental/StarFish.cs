﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: StarFish 
/// </summary>
[RequireComponent(typeof (BoxCollider2D))]
[RequireComponent(typeof (AudioSource))]
public class StarFish : MonoBehaviour {
	#region Fields

	#endregion

	void Start() {
		GetComponent<BoxCollider2D>().isTrigger = true;
	}

	void Update() {}

	void OnTriggerEnter2D(Collider2D other) {
		GameObject.Find("Hero").GetComponent<HeroMovement>().mana += 8;
		GetComponent<AudioSource>().Play();
		Destroy( gameObject, GetComponent<AudioSource>().clip.length );
	}
}