﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: SpawnLocation 
/// </summary>
[RequireComponent(typeof (BoxCollider2D))]
public class SpawnLocation : MonoBehaviour {
	#region Fields

	public string playerObjName = "Hero";

	#endregion

	void Start() {
		GameObject.Find(playerObjName).transform.position = transform.position;
		GetComponent<BoxCollider2D>().isTrigger = true;
	}

}