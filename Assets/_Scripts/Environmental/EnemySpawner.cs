﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: EnemySpawner 
/// </summary>
[RequireComponent( typeof( BoxCollider2D ) )]
public class EnemySpawner : MonoBehaviour {
	#region Fields

	#endregion

	void Start() {
		GameOptions.s.spawners.Add(this);
		GameOptions.s.StartSpawnCounter();
		GetComponent<BoxCollider2D>().isTrigger = true;
	}
}