﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: RedShroom 
/// </summary>
[RequireComponent( typeof( BoxCollider2D ) )]
[RequireComponent( typeof( AudioSource ) )]
public class RedShroom : MonoBehaviour {
	#region Fields

	#endregion

	void Start() {
		GetComponent<BoxCollider2D>().isTrigger = true;
	}

	void Update() {}

	void OnTriggerEnter2D(Collider2D other) {
		GameObject.Find("Hero").GetComponent<HeroMovement>().health += 15;
		GetComponent<AudioSource>().Play();
		Destroy( gameObject, GetComponent<AudioSource>().clip.length );
	}
}