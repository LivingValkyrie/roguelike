﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: FogTile 
/// </summary>
[RequireComponent(typeof (BoxCollider2D))]
public class FogTile : MonoBehaviour {
	#region Fields

	float opacityAfterEntered = 0.7f;

	#endregion

	void Start() {
		GetComponent<BoxCollider2D>().isTrigger = true;
		GetComponent<BoxCollider2D>().size = new Vector2(1, 1);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Hero") {
			//print("test");
			//Destroy(gameObject);
			SpriteRenderer temp = GetComponent<SpriteRenderer>();
			temp.color = new Color(temp.color.r, temp.color.g, temp.color.b, 0f);
		}
	}

	public void OnTriggerExit2D(Collider2D other) {
		if (other.name == "Hero") {
			//print("test");
			//Destroy(gameObject);
			SpriteRenderer temp = GetComponent<SpriteRenderer>();
			temp.color = new Color(temp.color.r, temp.color.g, temp.color.b, opacityAfterEntered);
		}
	}
}